# How to contribute?

We strive to welcome all contributors!

We want to be a project where contributing is both simple and inclusive, so please read the [Code of Conduct][code_of_conduct] (tldr: "We pledge to act and interact in ways that contribute to an open, welcoming, diverse, inclusive, and healthy community.") first.

[code_of_conduct]: CODE_OF_CONDUCT.md

### Table of contents

1. [Contribution guideline](#contribution-guideline)
    * [Git only](#git-only)
    * [Mutualized Open edX](#mutualized-open-edx)
    * [Self-hosted Open edX](#self-hosted-open-edx)
    * [Writing style](#writing-style)
2. [Organization](#organization)
    * [Meetings](#meetings)
    * [Communication tools](#communication-tools)
    * [Merging rights](#merging-rights)
    * [Decision process](#decision-process)
    * [Continuous deployment](#continuous-deployment)

## Contribution guideline

There are several ways to edit the course content depending on the significance of the change and
your technical requirement. The course being based on Open edX, you may use an instance to make
modifications.

### Git only

For trivial changes, you could modify the course via the raw content in the repository folders.

It could be the easiest way to fix a spelling error or modify a sentence without using an Open edX instance, but for more
substantial changes it's more convenient to use Open edX.

### Mutualized Open edX

The draft of the course is hosted on an [Open edX instance](https://courses.opencraft.com/courses/course-v1:MOOC-FLOSS+101+dev/course/) provided by OpenCraft, using it could be the most beginner-friendly way to contribute to the course, if you want to make larger changes.

In order to edit the content, you have to 1/ [create a new account](https://courses.opencraft.com/register) on this Open edX platform and 2/ ask a core maintainer
to grant staff access to your account, by creating an issue indicating the email address you have used to create your account. Note that if you don't wish to have your email visible publicly, you can check the option "This issue is confidential and should only be visible to team members with at least Reporter access." One of the maintainers will grant you access to Studio, the web interface used to edit Open edX courses.

To save the created content on Gitlab, export the course from the studio mode using the `Tools` > `Export` section.

> Warning : Content written on the Open edX plateform that is not saved on Gitlab may be deleted. Be sure that the content ends up on
GitLab, and if you're lost do to it, create an [issue](https://gitlab.com/mooc-floss/mooc-floss/-/issues) to ask another contributor to do it for you.

### Self-hosted Open edX

For advanced contributors, you can also set up your own Open edX instance. The recommended way to install is via its Docker-based distribution, [Tutor](https://docs.tutor.overhang.io/).

See also the official documentation: https://edx.readthedocs.io/projects/edx-installing-configuring-and-running/en/latest/

### Writing style

To unify the learner experience, there are some conventions to follow when writing parts of the MOOC:

* **How to address the learners**: when asking to do a task, use "you" (eg. "Go edit a page on Wikipedia! You should make sure to not post spam."); but for any other case where it feels appropriate, prefer to use "we", as it tends to be more inclusive: instead of being the teachers who tell students what to do, we put ourselves all in the same group/community, of contributors to free software projects (eg. "When we want to test a branch we'll typically not just read the code but also build it locally to test if the new feature works").
* **Neutrality**: we aim to remain neutral on topics that can be divisive within the free & open source community (for example, we don't chose between using "free software" or "open source" - we use "FLOSS" for "Free Libre Open Source Software"); when there are several possible point of views on such a topic, we will aim to present the different point of views, from the perspective of those who would defend it.

## Organization

### Meetings

We tried to create this course as openly as possible, so that even after the start of the project, it's still possible to understand the plan and the reasoning behind decisions taken. In that spirit, all our meetings are recorded and transcribed. In particular, we had two preparatory sets of live meetings about this course:

 - From February 2021 to March 2021, we brainstormed the contents of the course, aka "What are we going to talk about? How to approach it?"
 - In April and May 2021, we tried to put these thoughts into a *structure* for the course: What steps will the students have to take, what sort of activities will they do, and what we will expect them to learn.
 - From there, we are now (May 2021) focused on creating a first draft of the content, for all modules, and all module chapters. The first draft will be created in a `${week}/brainstorm.md` file, then split into the actual course structure within `course/html/` folder.

Follow scheduled meetings [here](https://gitlab.com/mooc-floss/mooc-floss/-/issues/40).

### Communication tools

We communicate primarily on three platforms:
 - GitLab: All long-running discussions happen here on GitLab, whether it's for discussing a problem or a planned change (in an issue) or discussing a merge request.
 - Video chats: Most of the brainstorms and plans we've made were organized by videoconference, which is usually faster to communicate and exchange ideas than text. Since it's harder to backlog, they are all transcribed to be browsable and searchable. The times and links of those meetings are usually announced as an issue on GitLab with a deadline.
 - Matrix: We have a public chat room for more informal discussions or to exchange links, but it's less used than the other solutions. The room id is #mooc-floss:matrix.r2.enst.fr.

Communication primarily happens in English (as the course will be in English). Some French may slip in as many course creators are French.

### Merging rights

All the [members of the project][members] (core committers) have the right to approve and merge the merge request. A new core member can be elected to the group by unanimous election in the current core group.

The merge request raised should be approved by at **least** one core committer.

A core committer can self-approve and merge a Merge Request _only if no objections are raised for a week_.

[members]: https://gitlab.com/mooc-floss/mooc-floss/-/project_members

### Decision process

Ideally, the merging decisions will be taken based on [Consensus][consensus]. The main difference with Wikipedia is that the modifications are approved before they are merged, while wiki editing is (mostly) post-publication consensus building, with the MR page as discussion space.

In the case of major disagreements, ultimately, **for now**, the decision process ends with the <abbr title="Benevolent Dictator For Life">BDFL</abbr> process from the initiators of the project (i.e. Marc Jeanmougin), but once/if the community grows, more democratic processes could be discussed and adopted, if deemed beneficial for the project's health.

[consensus]: https://en.wikipedia.org/wiki/Wikipedia:Consensus

### Continuous deployment

To update the course, a merge request should be raised against the default branch and get merged.
Once the changes land in the default branch, the `publish-course` CI job is triggered and should do the following things: log in to the Open edX platform, create an archive and import it.

To publish the course, the `publish-course` job is using multiple Gitlab CI variables defined in Gitlab settings.

- `EDX_STUDIO_URL`: Open edX studio URL, for example: `https://studio.courses.opencraft.com`
- `EDX_LMS_URL`: Open edX LMS URL, for example: `https://courses.opencraft.com`
- `EDX_AUTH_CLIENT_ID`: Open edX OAuth2 application client ID (under `admin/oauth2_provider/application`)
- `EDX_AUTH_CLIENT_SECRET`: Open edX OAuth2 client secret
- `COURSE_ID`: Course ID , for example: `course-v1:MOOC-FLOSS+101+2021_1`
