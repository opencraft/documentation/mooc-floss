0
00:00:00,080 --> 00:00:06,000
I like the sense of community that it

1
00:00:03,080 --> 00:00:08,320
brings because I&#39;m part I feel like

2
00:00:06,000 --> 00:00:11,880
after contributing I&#39;m part of the

3
00:00:08,320 --> 00:00:15,360
project I&#39;m discussing with this these

4
00:00:11,880 --> 00:00:17,680
people uh every week so so that we can

5
00:00:15,360 --> 00:00:20,080
decide like discuss what are the

6
00:00:17,680 --> 00:00:22,880
prospects that we are having for the

7
00:00:20,080 --> 00:00:25,400
project what we are doing going to do

8
00:00:22,880 --> 00:00:27,880
and basically discussing what the

9
00:00:25,400 --> 00:00:30,320
project will become should

10
00:00:27,880 --> 00:00:33,280
become these people are friends to me

11
00:00:30,320 --> 00:00:37,840
know so making friends over the project

12
00:00:33,280 --> 00:00:42,079
over contributing to a common thing and

13
00:00:37,840 --> 00:00:44,440
that we all feel is uh beneficial to the

14
00:00:42,079 --> 00:00:48,440
world and to its users and to everyone

15
00:00:44,440 --> 00:00:51,360
that can use it and be able to draw

16
00:00:48,440 --> 00:00:55,440
things or to do things I think that&#39;s

17
00:00:51,360 --> 00:00:55,440
very rewarding

