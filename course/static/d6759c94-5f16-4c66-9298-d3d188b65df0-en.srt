0
00:00:00,080 --> 00:00:03,799
I was always curious about Linux how it

1
00:00:02,159 --> 00:00:06,120
works because I went to a free software

2
00:00:03,799 --> 00:00:07,919
conference so that was where my

3
00:00:06,120 --> 00:00:09,679
interesting started then I met someone

4
00:00:07,919 --> 00:00:11,920
on the internet who said let&#39;s meet in

5
00:00:09,679 --> 00:00:14,599
this conference and then there I met

6
00:00:11,920 --> 00:00:16,520
other people and to be honest for me um

7
00:00:14,599 --> 00:00:18,039
so I started with open source in my

8
00:00:16,520 --> 00:00:20,880
dressing open source because I want to

9
00:00:18,039 --> 00:00:22,880
know how it worked but then I honestly I

10
00:00:20,880 --> 00:00:25,080
stayed because of the people I really

11
00:00:22,880 --> 00:00:29,080
connected with the community the people

12
00:00:25,080 --> 00:00:31,439
I met everyone I was very a lot of very

13
00:00:29,080 --> 00:00:33,120
nice people very principled people about

14
00:00:31,439 --> 00:00:35,200
you know open knowledge about sharing

15
00:00:33,120 --> 00:00:37,840
knowledge about create technology to

16
00:00:35,200 --> 00:00:37,840
help people

