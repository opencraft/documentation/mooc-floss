0
00:00:05,120 --> 00:00:13,519
my first contribution I was um promoting

1
00:00:08,960 --> 00:00:15,360
KD in Brazil I&#39;m Brazilian and um so I

2
00:00:13,519 --> 00:00:17,920
started by giving talks and small

3
00:00:15,360 --> 00:00:19,320
conference that we had locally my first

4
00:00:17,920 --> 00:00:22,080
contribution essentially was not even

5
00:00:19,320 --> 00:00:24,039
cold I always thought that even though I

6
00:00:22,080 --> 00:00:27,039
have a technical background but as

7
00:00:24,039 --> 00:00:29,800
someone from a minority in open source

8
00:00:27,039 --> 00:00:31,880
uh I always thought was very scary so

9
00:00:29,800 --> 00:00:34,840
yeah I started giving talks about how KD

10
00:00:31,880 --> 00:00:37,960
Works what is KDE the new releases they

11
00:00:34,840 --> 00:00:40,879
had features and things like that and

12
00:00:37,960 --> 00:00:44,640
much later then I join Google summer of

13
00:00:40,879 --> 00:00:48,600
cold so then I got a mentor we work on a

14
00:00:44,640 --> 00:00:50,680
project but then um it was all about

15
00:00:48,600 --> 00:00:53,559
people like I met the right people at

16
00:00:50,680 --> 00:00:56,160
the right time in the conference so I

17
00:00:53,559 --> 00:00:57,600
think I would tell people you know tell

18
00:00:56,160 --> 00:00:59,199
everyone like if you&#39;re interested in

19
00:00:57,600 --> 00:01:01,600
open source you should find a nice

20
00:00:59,199 --> 00:01:03,080
community and if they&#39;re not nice to you

21
00:01:01,600 --> 00:01:06,280
look for the next

22
00:01:03,080 --> 00:01:08,560
community and there&#39;s so much that you

23
00:01:06,280 --> 00:01:12,119
can do with open source not only cold

24
00:01:08,560 --> 00:01:13,920
you can help promoting as I&#39;m as I did

25
00:01:12,119 --> 00:01:17,320
you can help with translation you can

26
00:01:13,920 --> 00:01:20,360
help with design accessibility usability

27
00:01:17,320 --> 00:01:20,360
we need so much of

28
00:01:26,360 --> 00:01:29,360
that

