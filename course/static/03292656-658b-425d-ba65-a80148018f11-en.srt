0
00:00:00,120 --> 00:00:07,200
my my biggest road block was that I had

1
00:00:03,399 --> 00:00:11,240
no idea how like how to fix stuff in a

2
00:00:07,200 --> 00:00:13,679
big code base like I did some CS in my

3
00:00:11,240 --> 00:00:17,359
studies so I know how to write a small

4
00:00:13,679 --> 00:00:20,680
program I know how to read some code but

5
00:00:17,359 --> 00:00:23,000
what I learned in my uh studies was how

6
00:00:20,680 --> 00:00:26,880
to write small programs how to fix

7
00:00:23,000 --> 00:00:31,359
problems things that did not involve uh

8
00:00:26,880 --> 00:00:35,360
big code bases so what while I know I

9
00:00:31,359 --> 00:00:37,320
know how to write a program that fixes a

10
00:00:35,360 --> 00:00:40,520
small issue that I can do for instance

11
00:00:37,320 --> 00:00:43,960
studies with some data I can pass stuff

12
00:00:40,520 --> 00:00:45,879
and I can do very small contained things

13
00:00:43,960 --> 00:00:48,239
but when if you give me a project at

14
00:00:45,879 --> 00:00:50,680
this point if you gave me a project like

15
00:00:48,239 --> 00:00:52,719
inkscape with a few hundred thousand

16
00:00:50,680 --> 00:00:54,760
lines of code I don&#39;t know how to build

17
00:00:52,719 --> 00:00:56,640
it with if I don&#39;t find the

18
00:00:54,760 --> 00:00:59,359
documentation I have no idea what a

19
00:00:56,640 --> 00:01:01,440
build system is because I just and I was

20
00:00:59,359 --> 00:01:05,680
just used to just call a compiler on one

21
00:01:01,440 --> 00:01:07,960
source file um I have no I have no

22
00:01:05,680 --> 00:01:11,040
knowledge how I have no knowledge how to

23
00:01:07,960 --> 00:01:14,759
deal with big stuff and where to find

24
00:01:11,040 --> 00:01:17,560
information oh what is the lines of code

25
00:01:14,759 --> 00:01:21,159
that prints some stuff on the screen

26
00:01:17,560 --> 00:01:23,920
that moves elements ET it&#39;s just so

27
00:01:21,159 --> 00:01:27,040
overwhelming to think about huge code

28
00:01:23,920 --> 00:01:29,320
bases so that was a big Ro block for me

29
00:01:27,040 --> 00:01:30,439
my strategy was to stay around and try

30
00:01:29,320 --> 00:01:32,759
to

31
00:01:30,439 --> 00:01:35,320
every part of the code base until I know

32
00:01:32,759 --> 00:01:39,840
what&#39;s going on there or try to find

33
00:01:35,320 --> 00:01:42,200
actually the logic of the whole thing um

34
00:01:39,840 --> 00:01:44,360
and I got the logic by basically staying

35
00:01:42,200 --> 00:01:45,880
around and hearing people talk about it

36
00:01:44,360 --> 00:01:47,920
and I don&#39;t think that&#39;s the most

37
00:01:45,880 --> 00:01:50,600
efficient way basically the most

38
00:01:47,920 --> 00:01:53,320
efficient way would with some

39
00:01:50,600 --> 00:01:56,799
perspective be documentation like good

40
00:01:53,320 --> 00:02:00,600
documentation would have helped me uh to

41
00:01:56,799 --> 00:02:00,600
find my way around in the project

