#!/bin/bash

# launch this script in the "course/" folder to rename the whole file structure
# according to the mooc order
# TODO: use meaningful filenames

mkdir course2 course2/html course2/vertical course2/chapter course2/sequential

cha=0
for chapter in `cat course/course/2021_1.xml | grep chapter | cut -d'"' -f2`; do 
  ((cha=cha+1))
  printf -v chap "%02d" $cha
  newchap=chap-$chap
  se=0
  for sequential in `cat course/chapter/$chapter.xml | grep sequential | cut -d'"' -f2`; do
    ((se=se+1))
    printf -v seq "%02d" $se
    newseq=chap-$chap-seq-$seq
    ve=0
    for vertical in `cat course/sequential/$sequential.xml | grep vertical | cut -d'"' -f2`; do
      ((ve=ve+1))
      printf -v ver "%02d" $ve
      newver=chap-$chap-seq-$seq-ver-$ver
      h=0
      for html in `cat course/vertical/$vertical.xml | grep '<html url' | cut -d'"' -f2`; do
        ((h=h+1))
        printf -v ht "%02d" $h
        newhtml=chap-$chap-seq-$seq-ver-$ver-html-$ht
        sed -i 's/'$html'/'$newhtml'/' course/vertical/$vertical.xml
        sed -i 's/'$html'/'$newhtml'/' course/html/$html.xml
        find course2/html -type f -exec sed -i -e 's,/jump_to_id/'$html',/jump_to_id/'$newhtml',g' {} \;
        find course/html -type f -exec sed -i -e 's,/jump_to_id/'$html',/jump_to_id/'$newhtml',g' {} \;
        git mv course/html/$html.xml course2/html/$newhtml.xml
        git mv course/html/$html.html course2/html/$newhtml.html
      done
      sed -i 's/'$vertical'/'$newver'/' course/sequential/$sequential.xml
      git mv course/vertical/$vertical.xml course2/vertical/$newver.xml
    done
    sed -i 's/'$sequential'/'$newseq'/' course/chapter/$chapter.xml
    git mv course/sequential/$sequential.xml course2/sequential/$newseq.xml
  done
  sed -i 's/'$chapter'/'$newchap'/' course/course/2021_1.xml
  git mv course/chapter/$chapter.xml course2/chapter/$newchap.xml
done
rm -fr course/vertical course/html course/chapter course/sequential
git mv course2/* course/
rmdir course2
