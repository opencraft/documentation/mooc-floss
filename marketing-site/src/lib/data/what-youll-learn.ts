const whatYoullLearn = [
  {
    text: 'Getting pull requests merged on third party FLOSS projects',
    image: {filename: 'pull-requests.svg', alt: 'illustration of a pull request'},
  },
  {
    text: 'How to become a valuable member of the FLOSS community',
    image: {filename: 'floss-community.svg', alt: 'Illustration of a group of people'},
  },
  {
    text: 'Best practices for cooperating and collaborating effectively',
    image: {filename: 'collaboration.svg', alt: 'Illustration of holding hands'},
  },
  {
    text: 'The tools and methods to work on FLOSS projects',
    image: {filename: 'floss-tools.svg', alt: 'Illustration of a wrench and screwdrive'},
  },
  {
    text: 'Wisdom from high-profile figures in the FLOSS community',
    image: {filename: 'community-wisdom.svg', alt: 'Illustration of owl'},
  },
  {
    text: 'FLOSS licensing and monetization models',
    image: {filename: 'floss-licensing.svg', alt: 'Illustration of a licence'},
  },
];

export {whatYoullLearn};
