export enum MenuState {
  Closed = 'menu-closed',
  Open = 'menu-open',
}
