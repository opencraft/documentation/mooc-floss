# Brainstorm - Module 6 - Meeting 2 Transcript

Video: https://digimedia1.r2.enst.fr/playback/presentation/2.0/playback.html?meetingId=73d3cd53ec8ede3f636b9a782ecc9f6375cf2271-1621270528362

Marc: 00:00:02.416 Okay. So the goal of the sixth module is to have the students fix a bug in the project they've chosen. So hopefully, in the weeks before, they've chosen a project, and they've chosen a bug to work on, ideally by discussing with the project, and now, the idea is that we should teach them what they need to know in order to be more efficient in fixing a bug in a big unknown codebase, basically. So general purpose tools, like how to use Git efficiently to find the source of the bug, always Git Blame, Git Log, Git Grab, etc., or how to efficiently use an IDE, in general, if that's appropriate for the project, or anything really that would help them achieve this goal. So this study open-ended, and I don't have readily available ideas of activities that would be beneficial to that. But most of the time, they will spend in this week will be on their project because it's about the project and the bug in this week specifically. It's the last week before summing-- the next week is basically summary, what to take away, how to stay in touch with the project, and how to continue. This is basically fix the bug and submit a merge request and get reviews.
[silence]

Olivier: 00:02:06.425 So I was merely asking for kickstarting the meeting but we could, nevertheless, say hi to everyone.

Remi: 00:02:16.909 Yeah. Hello, everyone.

Marc: 00:02:20.209 So Remy, what do we think we should do in this week? What would you think students should do?

Remi: 00:02:30.240 Let me think. Do we have the transcript of the brainstorm? Yes. One month ago, it was uploaded, so.

Olivier: 00:03:02.931 So after let me think, let me read.

Remi: 00:03:06.483 Yes.

Olivier: 00:03:08.249 I'm joking, but I didn't do my homework either, so.

Remi: 00:03:11.589 Yes, yes, yes. Different kind of bugs. The good bug to fix, the reproducible or non-reproducible, the bad bug, the good bug, how do you define bad, good?

Marc: 00:03:33.108 That's already something that we discussed last week. Because it was in the project tips of the module five that they should find bug, and so we give tips on how to identify what's a good bug, that they can work on, and what's a bug that they will probably fail on.

Remi: 00:03:57.015 How to discover a new codebase, big one?

Marc: 00:04:02.491 Such a spin topic.

Olivier: 00:04:06.014 So I have a question, Mark? Why did you mention Git Grab, for instance, instead of plain grab?

Marc: 00:04:15.086 Because Git Grab is more powerful. I always use Git Grab when I searched for strings in the codebase, for instance, so that I know in which it's-- I find easier.

Olivier: 00:04:30.593 But I don't know a Git Grab because I was thinking that maybe-- so that can be something in the flat codebase, or there can be some search in history, which is bit different. So does it provide both, or is it just a grab?

Marc: 00:04:54.186 No, it's grab, but for a versioned file?

Olivier: 00:04:58.967 Okay. So you search only in versioned file? Okay. I see.

Marc: 00:05:11.752 You might be able to choose into history. I'm not sure.

Olivier: 00:05:16.946 Yeah, because like a Git bisect and stuff like that, it's bit more complex, but depending on the--

Marc: 00:05:23.066 Git bisect is very interesting, but I don't think it searches in history.

Olivier: 00:05:30.361 So is there any content to watch and read and learn, or, is it mainly particle application week?

Marc: 00:05:47.544 It's what we want. This meeting is about deciding what we want the students to do. So could be both.
[silence]

Marc: 00:06:08.097 But it will end with a task to do, which is, fix the bugs that you selected and--

Olivier: 00:06:17.274 At least contribute for potential fix?

Marc: 00:06:20.875 Yeah. So try to fix it and suggest a fix, and whether it's a good fix or not, it's up to the maintainers, but.

Olivier: 00:06:31.418 Yes.
[silence]

Remi: 00:06:44.188 Maybe first, you could discuss what you are trying to do with the community. And someone that knows the codebase better than you could reply. "Or if you want to fix this, maybe you should take a look at this and this or that or do it this way." Or maybe you won't have any answer.

Marc: 00:07:19.306 That entirely depends on the level of communication they've established with the project. But if they've asked for a bug suggestion, to the channel, to the discussion channels. It will either be, you can try this bug. It's a simple one. It's a simple one. You will need to do X and Y. Or it will have or not have any indication it will depend on, and if they haven't asked for help or discussed it. But just look at the bug tracker, they can try to find something and will not have helped if they didn't say they will work on it. Of course, we encourage communication, but we cannot expect that everyone will do it with maximum communication, but.

Remi: 00:08:18.575 Also, what could happen is you make pull request, and then a huge conversation starts from that pull request.

Marc: 00:08:29.583 If it's huge, then it should have happened before the pull request probably, depends.

Olivier: 00:08:40.063 I think if there is a conversation, it's partly successful. If the contribution is noticed and maybe it's not perfect, maybe the reviewer are a bit picky, and of course, you don't know all the details and so on, but that's how you learn, so.

Marc: 00:08:59.813 If you only get remarks about cut styles and the indentations, then it's probably good.

Olivier: 00:09:14.396 I'm not sure there is something generic enough to be addressed here because many projects and many bugs may be so different, so.

Ildiko: 00:09:32.127 I agree with that statement, I mean, projects being different. And even when people [inaudible] bugs with low-hanging fruit and simple, sometimes, they turn out not to be that simple. So even those scenarios are not really bulletproof unless you personally know that you checked that bug, and that particular bug, it's really easy to fix. Okay. Not talking about typo fix, but code-related things, sometimes, they turn out that, "Oh, it affects this and that too. Maybe we should think about this a little bit more." So you can't really guarantee even that, that the bug fix will be simple for someone who doesn't know the codebase at all.

Olivier: 00:10:21.762 The codebase or the toolchain or the--?

Ildiko: 00:10:25.317 The combination of all those, yes. And also, maybe they started to introduce themselves within the project, but depending on how the community is structured and the people who are more knowledgeable about the particular codebase that they are trying to touch, it also kind of varies how soon they get a response, how easy or complicated those people are and how easy or complicated it is to navigate through discussions and review and all that. So yeah, one part is the code part, and the other part is the human part, and the tooling and everything, and if you put all that together, it's just hard to kind of address this in a generic.

Remi: 00:11:21.868 How do you generally evaluate the efficiency of your training for Upstream University?

Ildiko: 00:11:33.928 So we, I think, basically, cut the bug fixing part, and we are trying to find issues in the training material. So either the slides because very often, we forget to update a link, or we still have a typo that's in and we haven't seen. We're also highly dependent on the contributor guide for reading material during the course, and that's a longer amount of text, so a bigger chance that they will find typos or links that are not working or something to fix. So we're trying to encourage them to fix those during the training. And as we usually have at least two people who have approved powers over those repose, then we can even review and get the fixes merged during the course. And for more code-related parts, when we are kind of lucky enough, we have mentors from those project teams that the students, who are interested in the coding part, are interested in. And we are rather focusing on making the connection between them, between the community members and the training attendees and kind of trying to take it from there in the sense of, "So this is a person who you can reach out to when you start to have some more experience with the project and starting to contribute." And then practically, they have an entry point into that project team.

Ildiko: 00:13:29.158 So we are trying to build connections and maybe focus a bit more on the human and communication part because that's kind of harder to learn and make themselves comfortable with. Because the code part is, you sit down, and then depending on how much time you have, you can figure a big chunk out by yourself. And then once you're trying to integrate yourself into the community is when issues are kind of coming out because you usually know how to test the code, but you may not know how to reach out to the people who are maintaining that codebase. So we don't really have an efficient way to measure efficiency in that sense.

Remi: 00:14:27.911 Okay.

Ildiko: 00:14:31.917 So we are kind of trying to choose things that we have some control over and guide the students through that because then, we can show them the experience firsthand as opposed to trying to rely on people who may said, at some point, that they might help and tags and labels that they are using that those accurate. Don't have that good experience with those parts from earlier.

Remi: 00:15:07.120 So maybe one activity could be at least to try because it really depends on the project to find the developer documentation for the part of the project we think a bug is linked too. Well, that's really difficult. But I was thinking about, so discovering the codebase is not only browsing code, but also what's in the code, comments, even if there is a dev documentation, you can go more deeply, try to understand what's going on.

Ildiko: 00:16:07.458 What we also try to suggest to people is run the tests. So most parts, they have to have tests. Otherwise, no one will ever adopt that codebase because you're not able to validate and verify that the thing actually is stable. So in our experience, it's a really good exercise to try to run the test suite somewhere, their local machine or whatever hardware that they have access to where they can run the tests. And once they're familiar with that, you can suggest them to find open reviews for the part of the codebase that they are interested in, so like a bug fix. And download the patch and run the tests again, and then trying to find something smaller, remove the bug fix part, like the code change that fixes the bug and run the test again, and if they pass, then you don't have a test for the actual bug fix and code change. And then you already have a review comment to give to the person who uploaded the patch that, "Hey, by the way, you're not really testing what you just changed." And so these kind of exercises-- and I think I just-- anyway, I probably mixed it up. You need to remove the test and then run it again and see if it fails.

Ildiko: 00:17:51.511 Anyway, but the point is that if you suggest them to play with tests and try to check out reviews and run the tests for those and play with those, then you both encourage them to review code and code changes. That is a really amazing entry point into a community because it shows that you care. Review is also an activity that sometimes, you just have to force people to do because it's not the most fun part to review what others did. And they also learn a ton, really a lot. And I think that every activity that depends on the ability to test the code and the changes that others made and that they will do is the best start that they can have because then, they will have the tools that they will need to fix bugs, implement new features, whatever else. And you can also [crosstalk]--

Olivier: 00:19:01.126 Yeah, press one for--

Ildiko: 00:19:04.416 Sorry, go ahead.

Olivier: 00:19:06.403 No, I was just saying press one for the test provided. Of course, there is a test framework, and maintain test codebase or so.

Remi: 00:19:23.751 Is it possible that, in some projects, you have a developer environment that is not the same as the debugging environment? I don't know why I think this, but.

Olivier: 00:19:42.632 The one-time bugs are the more difficult ones. You need data to reproduce the context. You need configuration parameters, and so on. So depending on the nature of the project, like fixing a UI message or some easy-to-fix thing, can be easy to fix because it's not an algorithm problem. It's just you have to change a file and compile, and that's done, basically. Or it can be easy, but you need to have the proper data to exhibit the issue. That's why if you don't have a testing framework that allows you to load data in a database and process it and dump it, and so on, it can be a nightmare to reproduce. But that very much depends on the context and the kind of level of complexity of assembly, of modules, and so on. So basically, when you are in an environment where you have containers and stuff like that, it can ease reproducability because you can have some automation. But that's not always the case, in particular, if there is some level of interactive interface or protocol or whatever, so.

Remi: 00:21:14.895 So reproducing the bug is the module before, but that's a very good point indeed.

Olivier: 00:21:27.841 I was thinking about connectivity that was quite basic, but still, we could formalize it. That would be to take log of what you do, and maybe have what kind of next step you could have, a bit like the interactive fiction stuff we discussed two weeks ago. Yeah, I think it was two weeks ago. Like what could you expect to happen next. You need something, and you happen to wait for someone to review, or you need to ask someone where is what you're missing to reproduce, and you can check the accomplished steps that you passed, more or less. So it can be a bit boring and a bit hard sometimes, because there might not be anything but ask, as the next step, or maybe the response could be fuzzy, and okay, you're provided with four different options that you don't understand if they are valid the next steps. But having a kind of path of what you did and where you are, and where you expect to finish, which is probably different from project to project, like a project doesn't need to be compiled the project that you'll need to, to get data to produce, but still having a kind of a progress indicator.

Marc: 00:23:10.262 Yeah. A developer, basically?

Olivier: 00:23:12.824 It's a log, but it's also thinking about what the community needs to do for you. So it's, "Okay, should I now post a message, or should I do something? Okay, I will post a message, and then I'm waiting for an answer, or I am doing something in between." You could have multiple options, depending if someone is aware and responds. And maybe if they don't respond, you can keep on doing something like documenting or preparing a test or whatever. But I don't know if the [continuity?] is difficult probably to estimate. So of course, if you have documentation, say, you're to reproduce bugs, you need to do this and this and this, okay, you have a kind of path that is defined already, but if it's not as formal--

Marc: 00:23:59.589 We can give a generality like every hour you work on this, just stop, and you take two minutes to write what you've done, what you plan to do.

Olivier: 00:24:12.521 [inaudible] or quarter?

Marc: 00:24:17.194 I would say every day, but.

Olivier: 00:24:20.329 Yes. But this is for us, so we're working full-time. But here, our learners are not in the same situation. So maybe then will spend hours on it.

Marc: 00:24:32.582 Yeah. That's why I say every day, so.

Remi: 00:24:40.394 So the test cases are great, but sometimes as, Mark, you have experience with the Inkscape. Even big projects like Inkscape do not have as many tests as we think.

Olivier: 00:24:59.898 I write them. No. It's very hard to-- yeah.

Remi: 00:25:06.038 Well, I was thinking about the debugging environment. If they want to find the source of the bug, they have to tweak to do-- if they don't have test cases for that, maybe they should go--

Olivier: 00:25:28.821 I do a lot of printf() debugging.

Remi: 00:25:31.580 Okay, printf() or breakpoints and step-by-step execution, that's another-- so there are different ways of debugging, right: test cases, printf() debugging, the Real Debugger. And also, they could, as some activity, introduce, as you said before, slow changes, and see if something is changing for real. Because sometimes, you change the code, but it doesn't do anything, and you don't know why. That happens with JavaScript with big projects that are completely messed up.

Olivier: 00:26:16.622 It's because all the projects JavaScript is cached somewhere, and the cached [inaudible], and none of your changes do anything. It's same with PYC code.

Remi: 00:26:30.608 Maybe the first step would be to, of course, find if there is different part documentation, different part comments in the code, try to find what the code is doing, talk to someone, if possible, that knows this part of the code. Sometimes, it's difficult to know which parts you should dive in. So just to find the good place in the big codebase could be challenging. So a lot of things happening here. A lot of tips we should gather--

Olivier: 00:27:15.775 Maybe we could have just a set of recipes, and there is no other, and it's just like tags and some descriptions that could be searched. I don't know if the platform would allow to do that or if it needs to be hosted somewhere or whatever. For typical scenery, like your compiled program or an interpreted program, something which has test base or something which doesn't have, something where - I don't know - Grab is not sufficient because whatever. And so it's not kind of watch this and do. It's just try and do and then stop and go, find one or two that could apply and something which is more-- [inaudible] I don't know if this is popular niche. The problem is that we don't know the prerequisite; we don't know the competence level of people; we don't know if speaking about printf() debugging is understandable, so.

Marc: 00:28:41.140 Yeah. So prerequisite of the MOOC is knowing how to program in one language. So in--

Remi: 00:28:48.869 Anyway, if in printf() debugging, it means you've found a good place to put the printf(), which is, I think, one of the most difficult part to start. It knows that in the big codebase, you know where to go. Printf() debugging itself is not difficult. Testing, also, putting a new test, if there is a test environment already with some tests, you can just copy/paste. And so the very difficult part is to find the source of the bug. So we should focus on activities, so very small activities at the beginning that make the learner exprore the big codebase little by little and then focus more on the bug itself to find the source of the bug, if possible.

Marc: 00:30:05.381 All right. First try to reproduce the bug is something they've already supposed to be doing?

Remi: 00:30:12.260 Yes. Reproducing a bug is, as you said, only a matter of data inputs and everything that is compiled and ready to reproduce the bug. It's not really exploring the codebase and navigating [crosstalk]--

Olivier: 00:30:29.168 And most likely, it was fixing between, and you cannot manage to reproduce because someone fixed it already and the debugger isn't aware. So that's a common pattern that could happen, that people found the low-hanging fruit, but it's already eaten by someone and all [inaudible]. Okay. I don't know if that needs to be addressed in big flashing characters. "Don't despair if you cannot reproduce because it happens most of the time. But the bug tracker is just not up to date. I don't know. Goodbye, Remy.

Remi: 00:31:15.149 I don't know. Something very nice smelling, like a barbeque, and I wanted to know if it was just here outside.

Olivier: 00:31:24.580 Maybe it's fire down. [laughter]

Remi: 00:31:29.487 Unfortunately, nothing is outside, so I don't know where it comes from.

Olivier: 00:31:36.175 That's how to reproduce bugging.

Marc: 00:31:41.051 Personally, what I did when I wanted to learn Inkscape codebase is I took a big but not very difficult refractoring task. So I had to touch approximately 200 files, which I did not have to touch in depth, but I had to understand the logic very locally everywhere. And it allowed me to have an overview of many, many, many things, but that took weeks. So I would not recommend that.

Olivier: 00:32:16.385 And that might not be the typical motivation of most developers in the context of this MOOC. I was thinking about something. Say we have 10 recipes or tapes, or I don't know how to categorize. Be it videos or tutorial or a demo of Mark doing printf() for everywhere and cursing the well because he cannot reproduce - I don't know - resources. And we asked people to watch free either at [Hondon?] or because they have a hint. Because, as I say, we cannot have such able description of something like that. And maybe because it's the same language, for instance, as the product dictate. And then ask them to evaluate if it was useful or not. Maybe not because it doesn't apply or maybe not because it was not for-- I mean, there was a problem. They need more experience, or would need more time or something like that. It was too hard.

Olivier: 00:33:50.053 And then can we try and have something in a form where they all discuss about that, to comment to say, "I tried that. And after I started again, I figure out that, oh, it was not exactly the way it was described, but I managed anyway." And, "Oh you did that. Oh, I could do. Oh yes. It was very better tip." Something that could improve, so we prepare a set of typical situations, and then we expect the participants to improve on that. Maybe they don't have time to improve while fixing a bug, while running--

Marc: 00:34:39.233 Very good work. We already planned for discussions based off of the few people who are on the same project, like having small groups of people on the same open-source project and working on potentially different bugs but going around on the same problems probably.

Olivier: 00:35:12.578 I'm thinking about something that is actually important. We can prepare and have some kind of fixed amount of work to do to, and then they improve on that. Or I don't know if it's really pedagogic or if it's the best way to-- or if they want to play the game that far.

Remi: 00:35:37.889 So what I worry about is the difficulty to find at least one source file that you could just open and focus on. If you have thousands of files, you don't know where to start. So maybe what we could do is if they don't know where to start, they could just pick a random file and try to comment each of the lines that are quite specific to the algorithm and try to understand what it does, maybe something like this. Or a few lines, or-- I don't know. Commenting a few lines. That's just an idea like this.

Marc: 00:36:25.587 You mean for them or in order to commence a project?

Remi: 00:36:29.791 Yes, for them, and for the activity of the MOOC and to start something.

Marc: 00:36:34.958 Because in my opinion, if there is a big codebase and there is somewhere, someone who tried to say, "In this folders, things do that. In this folder, things do that." And if you try to find something that's related to this sort of actions, then look at the filenames and the filenames will indicate what they do. File names are usually a very good indication of what's inside. And if they aren't, then it's very, very bad sign for the project.

Olivier: 00:37:17.012 But at least, there is something known of use for me, most of the times, is that I'm using a French interface on my desktop and some of the applications I'm using, but the source code is in English. And sometimes, finding the translation and finding the correct file name because it's not the same name. So I have a problem with printing, but in French [foreign], so I will look for [foreign] and I will just find the translation file. So then I need to go backward and figure it out that oh, maybe it's in the print but see that my issue is located. So there can be tricks like that-- it might not be that obvious. I mean, [inaudible], so I don't know.

Remi: 00:38:09.904 Maybe one thing, I was thinking about logging and printing errors. Maybe they could try somewhere in the file, somewhere they know for sure this part of the code will be executed, and you put something to log or to print or to have something in the [crosstalk] or.

Olivier: 00:38:36.449 Yeah, that's a useful practice or tip or whatever, technique, but once you figure out which module or which unit and that's in a single execution thread. If the client had several that misbehaved, and you need to be-- either in the client or in the server. But beforehand, you need to figure out what part I should investigate because otherwise, you don't know. You could remove stuff in the server while it's happening in the client or vice versa, so.

Remi: 00:39:14.051 Maybe the first--

Olivier: 00:39:14.634 Then, again, it's a lot of different tips, so we can have a catalog of things we could describe, like okay, the tip about the language issue. So I reproduced it. Oh, maybe it happens only in a particular locale, so you have to figure out how to reproduce with a certain locale, which can be tricky. Or maybe it's the way around it, then your idea of commenting out stuff to see if it is more or less there. Something I'm using a lot is also strace to try and see what files are touched, like is there input, output? Is there a configuration file? I mean, something crashes, and I don't have a clue. So I could trigger GDB, or I could use strace or maybe strace.f sometimes because it's what is threaded or whatever. So there are a lot of different techniques, but something happens for C programs; something happens for Python programs; something happens for interface with GTK. But I'm not sure we can have a single--

Remi: 00:40:27.646 Maybe we can categorize, like debugging, tracing is a little bit different.

Marc: 00:40:33.902 We can have a catalog of tips that they could contribute to if they want, but we cannot cover every possible things. And most of the times, the best source of inspiration for what to do will be the project and the context they have with the project. But we can definitely provide general advice like what to do.

Remi: 00:40:59.862 I think that really, at the very beginning, trying to find the start of the algorithm and modifying the very start to have something printing in or logging or - I don't know - is a good start because you are not afraid that--

Olivier: 00:41:19.148 They can ensure that if you change something, you see the changes.

Remi: 00:41:23.246 Yes, yes, yes.

Olivier: 00:41:23.821 Whatever it is.

Remi: 00:41:24.937 You are not afraid. You have this big project, big codebase, but you are able to change the very beginning of the start of the project, and it do something, and you see that you have changed something.

Olivier: 00:41:37.419 Yeah. And I think it's also important to make sure that you can do that in the proper place like the head of a branch, like particular versions, bug fixes a branch, something like that. So yeah, maybe something quite generic. Yeah. Try to break something on purpose to change your message. Compare eventually, see your changes.

Remi: 00:42:07.540 So at the very start of the program, maybe. And then maybe after when the program is started and you do an action. So before doing any action, just starting the program. And at the booting of the program, you are able to do a change, and you see the change. And then after you do a certain type of action, maybe to be deeper in the code, and you are able to do-- just after this action, you have the ability to change the code, something like that.

Marc: 00:42:38.899 That answers the first activity we can ask. Makes a program crash and start.

Remi: 00:42:46.327 Oh, yes.

Marc: 00:42:48.201 It's not hard, but it requires them to write something at the right place.

Remi: 00:42:55.711 Or if the program is starting, make the program not starting.

Olivier: 00:43:01.288 I would prefer to ask for adding your name to the contributor's list or changing the title of a frame or a window or a dialogue or something about, so-- not to change the copyright, of course, because it's bad practice. But tagging your own name, "I was here." Okay.

Remi: 00:43:27.333 Yes, yes. That's a good idea, something visible. But the thing is, sometimes, it's difficult to-- I mean, if you do printf() debugging, maybe you do the printf and nothing happens because of redirection somewhere. I don't know. So even this could be difficult. Sometimes [crosstalk].

Olivier: 00:43:54.198 Yeah. Activating logs with the verbosity level that is required, like using the C locale or not. Or launching in single, stand-alone mode or something like that, which is don't [inaudible] and escape my control. Maybe there is no documented debugging environment and procedure, but there is still some common practices, depending on languages and stuff.

Remi: 00:44:32.668 Right. So I agree that we should, at the very beginning of this module, make them change their source code with something very simple, even if it is disconnected from a bug. And make sure that, if they change the code, they can see that something is happening because of that. And they have to prove it with screenshot, or. As a first activity, for me, it looks--

Olivier: 00:45:09.557 And then they need to know how to revert those changes when they will push the proposed change. Otherwise, you have a review where someone is tagging the code and fixing a bug at the same time, but. Okay. That's two different activites.

Marc: 00:45:33.195 Makes for a very confused reviewer.

Remi: 00:45:47.414 All right. So basically, we have a lot of ideas, right? Reviewing orders for requests, navigating through the code, the developer documentation, the comments, doing this first activity to change the code and knowing that it works, that it does something. We have to have a kind of bootstrapping, and once it's bootstrapped, they can go deeper.
[silence]

Marc: 00:46:31.478 In my opinion, we should have a start point like that to make them feel confident in the codebase. That they are actually able to edit a file, save, and run something after that. And as soon as they have this level of confidence, we should probably leave them to the fix and try to leave them to the project, basically. We do have this kind of generic advice for all the languages we can cover, but the best advice will probably be the one given by the project themselves. Like in this project, we do that to test. We do, "This is failing because of this module, and so this module is in this folder," so you can look at the files in this folder and see if one is doing the actions that is behaving badly with the file name, and then you look at the file name, and you find one that's relevant; you add a printf() or whatever inside, and you see what's executed and what's not executed, but that's--

Remi: 00:47:43.190 Also, another activity could be to look at closed issues and everything that happened. When did he start? How long did it take to fix the bug, and the history of the bug and stuff. If the bug is linked to--

Olivier: 00:48:07.130 Was this something that was part of previous weeks?

Remi: 00:48:12.755 Not really, actually. Maybe we could put that in Module 5.

Marc: 00:48:23.802 The previous weeks has build the project, and run the test, and run the project, but not modify anything. We had reproduced the bug too.

Olivier: 00:48:41.876 The level of details in the conversation of someone who's a beginner or someone who's an expert will be quite different, so learning from looking at a discussion in a bug tracker can be complex.

Remi: 00:49:06.789 But at least, it--

Olivier: 00:49:07.495 And as always, the issues of the power struggle, I would say, between users and developers with, "It doesn't work." "Yes, it does. You didn't reproduce the right way," and closing and reopening, and closing and reopening. So if people happen to be directed to such issues, it won't be very good for them to learn.

Remi: 00:49:40.160 So is there a way to have analytics of repositories on GitLab or GitHub, like the average time to fix a bug, or - I don't know - this kind of stuff?

Olivier: 00:50:03.007 Probably. But what can be interesting is to look at change logs, if they exist, and try to trace back what was changed. Maybe not the discussion on how it was changed, but just figuring out. If you compare two codebases, and you trace back to the comments, change logs and stuff, so you can understand what changed and if it fixed something. So maybe it's just like if you give two consecutive minor releases, and you have a kind of log of the changes, maybe you can-- apart from the [inaudible], just looking at tables, you can spot how people changed somehow. You don't know how to produce; you don't know how to compile, but you know that the code evolves in some places, and maybe those places correspond to an issue. So you can trace a bit like, "Oh, they changed something, and what did they touch? Oh, they touched those files. What for?" I don't know if it's an activity, but it could be something leading to an activity, provided that it's minor releases, because otherwise, it's too big and messy. But I don't know if it's something interesting, maybe.,

Remi: 00:51:38.878 Also--

Ildiko: 00:51:39.169 I think even do minor releases are too big.

Olivier: 00:51:42.524 Depends on the [inaudible].

Ildiko: 00:51:43.545 Back in the day, when I did some coding in some of the OpenStack projects, I remember that we were using Git and Gerrit. So we are iterating on a patch set. And when you did a rebase, then everything just got messed up in terms of you really didn't want to see what happens during the rebase. Because then, you didn't even know what you were changing in that patch set anymore. And it's not too minor base the same thing that you're trying to fix, and then you just rebased in the middle of the process. And you were like, "Damn, what is happening here now? I can't see what I changed, what others changed." And then you just don't know what's happening anymore. So I would advise against that because it's just too many changes. Even with minor releases in one file, it can be--

Marc: 00:52:42.593 We can look at one merge request or one commit. And usually, one merge request is change sets that makes sense and has some descriptions [inaudible]?

Ildiko: 00:52:57.045 Yeah.

Olivier: 00:52:57.630 It depends very much on the kind of programs, but.

Remi: 00:53:02.853 Also, I was thinking about something like dependency graphs. Sometimes, a bug depends on other bugs, and one bug depends on certain functionality of the program. And the functionality depends on the source code that is somewhere. And drawing charts graphically, dependency of something sometimes can clear things up.

Olivier: 00:53:34.475 Yeah. But again, it leads you to a complete software engineering course.
[silence]

Ildiko: 00:53:52.008 I'm kind of thinking that no matter which direction we want to take steps into, we will just get to a complete course in the particular programming language, depending on the project. If it's a bigger one, then in that project too, I think it's really easy to go down a rabbit hole here. Because I think that this is kind of the next steps part in terms of if someone picked a project that's written in C, that shouldn't be our responsibility to teach them how to program in C. If they can code in that language, cool. If they cannot, go and learn that first because no one in the community will teach them how to do that from scratch. That shouldn't be the expectation either. So we should make that clear that whichever project they chose, there are expectations and things that they should know. Or if they don't know that yet, that's cool, but they still have to go and learn it and this course is not addressing this and this and this and that. And it's probably good to remind people to that as well, that even when they've finished and then they feel empowered and enthusiastic, the coding part, if they want to code, then they still are going to have to learn that. And we can't really teach them just by pointing them to a low-hanging fruit bargain, encouraging them to run the tests because that will not teach them how to code in C.

Remi: 00:55:31.827 Yes.

Ildiko: 00:55:32.172 It will certainly not.

Olivier: 00:55:33.092 And maybe if I may, I think that too much emphasis on bug-fixing is not good. That's most of what we discussed today, and it's probably the hardest thing to do. And if you look at how projects start, people just contribute crappy stuff, but still, they are contributors, and they learn. And then they learn if people notice bugs in their code and they learn on the way. But it's not like you can contribute only by fixing bugs because fixing bugs is sometimes the hardest part. So the idea for adding something that is useless but is nice is probably easier than fixing a bug in something that works. So yeah, fixing bugs is probably hardest path.

Remi: 00:56:28.758 Well, if you have fixed a real bug and your code is merged, you have the best grade possible for the MOOC.

Olivier: 00:56:39.272 You have a contract.

Marc: 00:56:40.692 But yeah, if you suggest a contribution, like suggest a new feature, and that's not on the road map, then that's not really plan-based project and your merge request gets closed because no one really wants it. Then it's not really contributing.

Olivier: 00:57:00.236 Yeah, but at least you experienced some of the process, so you learned along the way, and of course, you shouldn't be annoying to the project. You shouldn't [crosstalk] both time.

Marc: 00:57:14.404 Yeah. I mean, bug-fixing is the easiest way to have something approved because it's a problem that's identified as a problem and already tracked and recognized as a problem.

Olivier: 00:57:29.656 Yeah, yeah I do agree, but--

Marc: 00:57:31.212 If they have a road map with planned feature and say "We don't have time to work on this, but this new feature would be great," and you want to work on that, then that's also great. But that's also covered usually by issues in bug trackers, like issues we cannot do that yet because it's not implemented, and a bug is not the only kind of issues that can be tracked in projects.

Ildiko: 00:58:00.984 I think that depending on the level of experience that people have, we can suggest different types of activities. Like if someone is extremely familiar with the programming language and has kind of a good idea of the project too, they can go and find a bug to fix. If someone is not that experienced, or they really just kind of would like to look around a bit more first, I would give them a review, goals, and activities. They can review others' bug fixes. They can run their tests. They can check if the test coverage is still up where it should be and hopefully even increased. If someone becomes an active reviewer before fixes a bug or adds a new feature, that's a lot of-- I believe that should come with a lot of respect from the active project contributors. Because again, reviewing is a hard labor, and I think most of the projects always complain that people are not reviewing enough. So that's really the best way to learn, and if we have an extra module where you still encourage and remind people how important it is, we cannot lose, and we cannot teach people or not encourage people to do anything that's not useful or that they will have an awful experience with.

Ildiko: 00:59:48.713 The other thing is that if the project that they have found for themselves is easy enough to deploy somewhere or small enough, or can run on their machine or something, it can also be a good idea to see what's the smallest service or project or the easiest deployment route, and have them set up an environment and play with the APIs. Try out how it works. I can almost really confidently guarantee that if they start to play with the APIs, they will find a bug. Someone probably reported it already, or maybe they didn't, but there's always an issue when you start to really explore how the part of the project works that you think or you know that you're interested in. And if the bug is not reported already, then they can go ahead, report the bug, and exercise how a good bug report looks like. And those are all activities that will make them learn about the project without pointing them to something that is in the middle of a potentially complex and late codebase that they picked for whatever reason, and then they feel obligated that they have to fix that particular issue.

Ildiko: 01:01:32.794 And then they will just go and rabbit-hole themselves without doing really the basics and exercise the basics until they really have an idea about the codebase and what the project does and how it actually works. Personally, I think that focusing on exercises like these is more beneficial for newcomers and newbies. Again, if someone is really experienced programmer and all that, then, yes, they should go ahead and find a bug they feel that they can fix. But if we suggest that to everybody, they will probably give it up after a few days because it's just complicated for them and maybe it's not for them on the first place. My two cents.

Remi: 01:02:25.378 And the title of the MOOC is Contributing to Free Labor Open Source Software. It's not really fixing a bug in first project. So yeah. I kind of agree with you. So do you think we should give a choice? I mean, so it should be clear from the beginning that-- because, Mark, you were not so sure about that. Mark, from the very start of the project, you said, "To get the certificates, you have to prove that you have changed somewhere something in the code." And that's the whole--

Marc: 01:03:21.104 [crosstalk] CID, yeah.

Remi: 01:03:22.324 Yes. So we do we give another chance of proving that it was too complicated, but still, I contributed a lot to the project because I did this and this and this. And we know because we have this publication on different types of contribution that the total amount of contribution is good enough so that they could have this certificate without changing a specific part in the code. I don't know.

Olivier: 01:03:57.553 I don't think it should be advertised. At this point, it's the end of the MOOC. We can say that code-related activities like code reviews, for instance, are meaningful contributions, and to take times, and if there are appreciated by-- but in my opinion, it's very hard to do a good code review without being able to code it instead. I don't know what you think of the code, but most of the time, the code reviews are done by people that are more knowledgeable than the person who code it.

Ildiko: 01:04:53.588 Well, you mean, in general or for section, or?

Olivier: 01:05:00.958 In general. Yeah.

Ildiko: 01:05:03.629 Probably. I mean--

Olivier: 01:05:09.187 I mean, most of the times, they're done by some sort of maintainers or persons that's been for some time in the project, so they obviously have some insight.

Ildiko: 01:05:21.276 Well, they review before it gets approved for sure, but we always encourage everyone to do reviews because if only the maintainers are reviewing, then the only thing that they do is reviewing. And that's not exactly the point of being a maintainer, that you're the only person who actually reviews a code change. That should be a shared responsibility. And therefore, it should not be the case, and that's why I also like to just repeat it all the time, how important reviewing is. Because then if it becomes a habit for someone, then they might just be a part of a team, and share all the responsibilities that they have. It never stopped me from doing code reviews either, that I've never been the most experienced person on any codebase in the open-source projects where I did code review work. And just with kind of spending the time to really review the change, even if I had really just small ideas, what that particular large codebase, how it worked, I was usually able to spot issues in the code changes that others uploaded for review even if they were really experienced people, just because you mistyped something, you just--

Olivier: 01:07:05.390 Yeah. You can see--

Ildiko: 01:07:06.939 --[crosstalk] is not 100%, and all that stuff. And it's really easy, actually, to spot small things that can be improved.

Olivier: 01:07:19.212 I agree that it's easy to spot things that can be improved or issues? But it's, in my opinion, very hard to assess that the code is doing what it claims to do, and in a good way. You can say, "This is bad," but saying, "This is good" requires much more understanding of what's being done?

Ildiko: 01:07:40.821 Yeah. Yeah. That's where the part of playing with the tests helps a lot, like checking if the tests are really testing the code change, best exercise ever for a newcomer, really. And you really often realize that it tests some part of it but not really the whole thing. And that's always a valid comment that your test is not really testing what you actually changed because that's always an issue, and that will cause an issue later.

Olivier: 01:08:21.111 But it's also only applicable to the dozen of fresh projects that have extensive testing including [inaudible] OpenStack probably because it's one of the biggest project I know.

Ildiko: 01:08:37.901 Yeah. I think it's also one of the more or most tested projects also. At least the course services are pretty well tested, but again, there is no project where you cannot improve testing.

Olivier: 01:08:55.127 Yes. Of course, but you usually have to write new test if you want to test something when it's testable when it's--

Ildiko: 01:09:03.819 Yeah. Although it would really concern me if I wanted to code somewhere and the project doesn't have tests. That'll be a red flag for me, I think, even though I'm not a hardcore coder, but.

Remi: 01:09:22.631 Sometimes, it's difficult to make tests for graphical or-- imagine - I don't know - a game where you have to test by playing the game, right?

Ildiko: 01:09:43.759 Probably not [inaudible] posted either. That would be the part of deploy the thing and try it out how it works. Yeah. I mean, again, depending on the project in terms of which path they can choose, testing it or deploy it and play with APIs or something. There must be something that they can do. If they cannot run tests, if they cannot write tests, if they cannot deploy and try it out, then how will they ever fix or add anything to it and be sure that it actually works.

Olivier: 01:10:33.662 I'm never sure. I just try things, and if it works better after, then it's probably good.

Ildiko: 01:10:42.039 Yeah. But that's the point, that you try. So there must be a way to try either automated testing or deploying and try. But there has to be a way to try. That was my point. Simply that. That was the only thing I wanted to get to, that we have to go to a point where they can try one way or the other in my opinion.

Olivier: 01:11:07.486 Yeah. It's--

Remi: 01:11:09.444 There's also the point of modifying the boots of the program and make it different just to try you're able to modify the source code somewhere.
[silence]

Olivier: 01:11:34.324 It's already 15 minutes to the end of the meeting, so maybe we should try to convert what we've said into elements and types of activities or types of things to put in the MOOC, basically.

Remi: 01:11:55.760 Also, do we want to have - I don't know if it exists - histories of bug fixes, famous bug fixes like, commented by someone.

Olivier: 01:12:15.470 You can have examples if you want, but I think that would be with your idea of a short video about what is printf() debugging. We can show, "Oh, here is a bug, and I will find the exact line where it happens by printf() before we're finding what's the state of the variable at some point." I think it can go with this idea, rather than being standalone.
[silence]

Marc: 01:13:14.952 All right. So I'm not sure how to implement it with--

Olivier: 01:13:18.798 Also, what do we grade?
[silence]

Olivier: 01:13:37.280 Is it--

Marc: 01:13:37.349 But for sure, the activity where they have to change the startup or the boot of the programming or put their names somewhere should be an activity that is graded.

Olivier: 01:13:54.840 And how do you grade it? The grading? Lots of peer grading takes time.

Marc: 01:14:04.312 Yes.

Olivier: 01:14:07.854 So if one of the activity is to do code reviews, and they also have to do peer grading of code changes [inaudible] that's also redundant.

Remi: 01:14:26.198 But the peer grading could be just looking at regularly to the book they create from the start. They have to grade the book of somebody else just to check if, indeed, something happened.

Olivier: 01:14:47.748 When you do peer grading, can you have a list of questions to answer about what you're grading, or is it when yes, no?

Remi: 01:14:58.252 You have to have-- so you have different questions, and for each question, you have different criteria, and each criteria gives you grades, and it's the average of all the grades. It's like a matrix; questions horizontally, criteria vertically, and then all the grades.

Olivier: 01:15:26.456 Okay. So we can just have a peer grading activity at the end after documenting what they've done in GitLab, and so they can see all the steps about this module for other people they are grading and check everything at once, right? They don't have to come back and forth.

Remi: 01:15:47.354 Yes. Although it would be easier to do it step by step, I think. Not the whole thing at the end. Maybe two or three steps.

Olivier: 01:16:02.989 But they will not-- if they do all at once, they will have the same project to grade, whereas if they do it step by step, they will end up with different people and different stuff to--

Remi: 01:16:16.712 Yes. And that's not bad at all.

Olivier: 01:16:19.999 Okay.
[silence]

Olivier: 01:17:09.866 So I am putting stuff into the [foreign].
[silence]

Olivier: 01:17:54.063 So we have, first, tips about finding different implementation. How to use Git to find stuff, how to do different types of debugging, how to use an IDE efficiently. We have some first steps that might be to make you confident that you can change a source code, like write a test. No, maybe write a test can be in there. Change something happening when you launch the program, like print something on the console, or whatever. And then we have the fix a bug activity which is solve the bugs that you've chosen. And then peer grading about documenting about what you've done. What else have we talked about? We have had lots of ideas, so I've probably missed some.
[silence]

Remi: 01:19:13.287 So yeah. It's a first shot, anyway.
[silence]

Olivier: 01:19:43.625 Polieta, what do you think about these steps? Do you think that's doable, or do you think that's too hard or too easy?

Remi: 01:19:55.514 It may be a little long to follow each of the steps, and maybe longer than if a student just was autonomous. I don't know if this is [inaudible], but there are maybe some students that are not autonomous, and in this case, adding some steps and tips is really necessary.

Marc: 01:20:37.596 So what we can do is start with, "If you're confident enough that you can solve the bug, please go directly to the third part where you just try to fix the issue, and submit your fix. And if you need advice on how to navigate or behave in a big codebase, then you can see the tips. And if you want to gain more confidence with codebase, you can go to the first steps in a codebase like write a test or change something that happens, or." Would that be--

Remi: 01:21:11.889 Yes, I think it's a good compromise.
[silence]

Olivier: 01:21:45.760 I agree that it might be a bit long, what do you think, Remy?

Remi: 01:21:50.243 No, I'm not afraid about being too long, especially if you-- I'm more afraid about struggling students, frustrating students, but not about the excellent one that will just go faster, and it's not a problem, I think.

Olivier: 01:22:18.234 Yeah, if right at module four, they already have discussed and thought of an issue and wrote a fix, then they can just skim and say, "Okay. This is good search, right? Okay. I can submit that." But yeah, if we have struggling students, we should always have some sort of discussion space where they can exchange with other people on similar projects. But the issue is for students that do not really communicate within the MOOC or within the projects, they will just stay stuck and probably just drop. If we have students that do not communicate either within the course or within their projects, then they will probably be completely stuck at this point.

Remi: 01:23:18.170 Yes.

Olivier: 01:23:20.207 Which is probably normal because you get that contributing is not something you can do completely alone.

Remi: 01:23:29.731 Maybe we can give them the ability to drop at any step or any module, but at least maybe we can ask them to write or to notify very briefly why they dropped. That could be nice.

Olivier: 01:23:59.205 How do you want to do that? Because I'm not sure it's a good idea to provide a button, "I want to drop now, and--"

Remi: 01:24:13.203 Why not?

Olivier: 01:24:15.134 Because it can create people to click on it.

Remi: 01:24:23.414 I don't know.

Olivier: 01:24:26.223 [inaudible]. But we can just put and then have a very long message about the 10 reasons why they should not drop and should continue and try again and harder. But no. Also, who should be the people in charge of this week, or of which parts of the week?

Marc: 01:25:10.859 I think I already have two of them.

Olivier: 01:25:15.030 You have two weeks?

Marc: 01:25:16.392 Yes. On June 2 and 5.

Olivier: 01:25:22.094 Yeah. And I have 3 and 6 or something-- no, 3 and 4, probably. No, wait. Yeah. No, 3. Oh, I just have 3, so I will add myself. Okay. I'm in charge of 6 module.

Marc: 01:25:46.572 Okay.

Olivier: 01:25:49.790 It's not the easiest one, which is fine. And I will put you up the first steps.

Marc: 01:26:02.970 Okay.

Olivier: 01:26:04.568 The goal is to--

Marc: 01:26:07.325 [inaudible].
[silence]

Olivier: 01:26:22.732 Then I will take the other parts, probably. Polieta, do you want to take on parts of the course, or Ildiko? I know, Ildiko, you're very busy, so maybe not.

Ildiko: 01:26:39.647 Oh, this week, especially. I will get vaccinated on Wednesday, so hoping I will survive this week.

Remi: 01:26:46.739 Oh, nice.

Olivier: 01:26:47.722 Yeah. You have greater odds now.

Ildiko: 01:26:52.314 Yeah. If I survive the vaccine, then I probably will be forever or something.

Olivier: 01:27:05.091 And Polieta, do you want to take on a part of this week?

Remi: 01:27:08.453 Yes. There was two part. I just deleted my tab. So the 3rd and the 4th, I think I can do.
[silence]

Olivier: 01:27:48.899 Okay. So do you have anything to add for this week? Otherwise, it's time. So I think I will have to re-listen to the whole recording because we've talked about lots of things today.

Remi: 01:28:12.648 Yes, and were syntheses of the transcript of the brainstorm. Maybe we had other ideas that we forgot.

Olivier: 01:28:26.496 Maybe. But I've read the transcript before starting the meeting, and there was lots of discussions but no precise stuff to do.

Remi: 01:28:35.873 All right.

Olivier: 01:28:41.135 Okay. So--

Remi: 01:28:42.580 Maybe when we create the contents of the other modules, we will get more ideas. We will get more precise for this one.

Olivier: 01:28:54.043 Maybe. Okay. Otherwise, it's time. So thank you, everyone, for attending. And thank you everyone for your ideas. And that's great.

Remi: 01:29:05.821 Yes.

Remi: 01:29:07.916 Bye.

Remi: 01:29:09.266 Thank you. Bye-bye. Have a nice weekend.

Ildiko: 01:29:10.941 Thanks. Bye.

Olivier: 01:29:12.172 Thank you. Bye. Okay.
