## Picking a location

Pick a place which has:
* Calm (no background noises)
* Neutral background, like a white wall
* Good lighting, and isn't backlit (no window or sun behind you)
* A desk or surface to support your computer and the camera

## Receive the hardware

You should have received through the post:
* A [lavalier microphone](https://www.amazon.es/gp/product/B0C393GWS3/) (with: mic, USB antenna)
* 3 USB-C adapters for the lavalier antenna: microUSB, iphone, 
* A holder [for the phone](https://www.amazon.es/gp/product/B074GQ4XZR/) or [for the camera](https://www.amazon.com/Overhead-Tripod-Camera-Flexible-Articulating/dp/B09P54Q3KV)
* [Optional] The [4K camera](https://www.amazon.com/AKASO-EK7000-Sports-Waterproof-Camcorder/dp/B01HGM33HG) (if your phone wasn't working out), with an SD card
* [Optional] In-ear headset, to allow to hear the interviewer without it being recorded

## Test the video conferencing on your computer

Your computer will only be used to allow you to interact with the interviewer. The recording itself will be done via your phone and the lavalier microphone that have been provided. So it's not too important to have good video and sound on the computer, but it will be important to have good video and sound recorded on your phone (see later sections).

* Go to https://digimedia1.r2.enst.fr/b/mar-vab-tqr enter your name and click "Start" to join a room
* Configure and test audio & video

## Plug the lavalier microphone

* Plug the receptor (T-shaped antenna) to the side of the phone, using one of the provided USB adapters if needed
* Attach the lavalier microphone to your t-shirt -- not too high, pointing up toward your mouth

## Setup the video recording

* Attach the camera holder to a table
* Attach the camera or phone to the holder:
* Position the camera horizontally (to record in PC/TV format, not like a Tiktok video!)
* Ensure the camera of the phone isaligned with the laptop screen, aiming the gaze at the position the interviewer will appear on the screen
* Set your video recording resolution to the highest available, ideally 4K, and with the highest quality (low compression)

## Test recording

Do a test recording on the phone, and then review the recording.

### Video check

Ensure that you are well positionned in the frame:
* Adjust it so that the video **captures your torso and hands**
* Keep a margin in the frame to never get out of it, no matter how much you move (in doubt, pick a larger frame - it's easier to fix by cropping during editing)
* Align the video frame to be horizontal to the ground
* Check that your eyes align with the camera of the phone when you look at the interviewer 

### Sound check

* Check that the lavalier is the one recording: 
	* by talking holding the lavalier while going away from the phone, and ensuring we can still hear at the same level on the recording
	* by talking close to the phone with the lavalier put away, and ensuring we can't hear that
* Check that the sound level is enough to hear you clearly, and that there aren't any background noises

